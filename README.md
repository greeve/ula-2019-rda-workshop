# ULA 2019 RDA Workshop Resources

Resources for the ULA 2019 RDA Workshop. The workshop is divided into two parts and is conducted by [Robert Maxwell](https://lib.byu.edu/directory/robert-maxwell/) and [Valerie Buck](https://lib.byu.edu/directory/valerie-buck/) from the [Harold B. Lee Library](https://lib.byu.edu) at [Brigham Young University](https://byu.edu).

- [Cataloging with RDA: Learning the Changes of the Updated Resource Description and Access PART ONE](https://ula2019.sched.com/event/LiDA/cataloging-with-rda-learning-the-changes-of-the-updated-resource-description-and-access-part-one)
- [Cataloging with RDA: Learning the Changes of the Updated Resource Description and Access PART TWO](https://ula2019.sched.com/event/LiJd/cataloging-with-rda-learning-the-changes-of-the-updated-resource-description-and-access-part-two)

# Files and Folders

- RDA cataloging Excel templates for the workshop are found in the `templates` folder. 
- Powerpoint slides for each part of the workshop are found in the `slides` folder.

# RDA Application Templates

The application templates in this repository are based on templates available from this Google drive location:

<https://drive.google.com/drive/folders/1E3Q_jpv61MUy4Gy1uAFI5n4LrITRO8yx>

The templates used for this workshop can be downloaded in a zip file from the following URL:

<https://bit.ly/ula-2019-rda-workshop>

# Submit for review

Email saved cataloging work using the RDA Excel templates to:

<ula_rda_workshop@byu.edu>
